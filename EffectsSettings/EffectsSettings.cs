﻿using BepInEx.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectsSettings
{
    public class EffectsSettings
    {
        public ConfigEntry<bool> DoGlitchwave;
        public ConfigEntry<bool> DoEclipseEffect;
        public ConfigEntry<bool> DoStartMoveDustEffect;
        public ConfigEntry<bool> DoSlowDustEffect;
        public ConfigEntry<bool> DoWalljumpDustEffect;
        public ConfigEntry<bool> DoLandDustEffect;
        public ConfigEntry<bool> DoDashEffect;
        public ConfigEntry<bool> DoJumpDustEffect;
        public ConfigEntry<bool> DoDoubleJumpDustEffect;
        public ConfigEntry<bool> DoHitBallColorEffect;
        public ConfigEntry<bool> DoSmashEffect;
        public ConfigEntry<bool> DoSmallSmashEffect;
        public ConfigEntry<bool> DoThrowEffect;
        public ConfigEntry<bool> DoCatchEffect;
        public ConfigEntry<bool> DoHitBallEffect;
        public ConfigEntry<bool> DoHitBallBehindEffect;
        public ConfigEntry<bool> DoWallHitEffect;
        public ConfigEntry<bool> DoDeadBallEffect;
        public ConfigEntry<bool> DoHighSpeedBallReleaseEffect;
        public ConfigEntry<bool> DoDeflectHit;
        public ConfigEntry<bool> DoGetHitEffect;
        public ConfigEntry<bool> DoKillPlayerEffect;
        public ConfigEntry<bool> DoGrabFailEffect;
        public ConfigEntry<bool> DoLightningStrikeEffect;
        public ConfigEntry<bool> DoBombExplodeEffect;
        public ConfigEntry<bool> DoSmokeEffect;
        public ConfigEntry<bool> DoImpactEffect;
        public ConfigEntry<bool> DoClashEffect;

        public ConfigEntry<bool> DoMovementEffects;
        public ConfigEntry<bool> DoBallEffects;
        public ConfigEntry<bool> DoDamageEffects;

        private bool hasInit = false;
        public bool DoEffect(EffectTypes type)
        {
            if (type == EffectTypes.GlitchwaveEffect)
            {
                return DoGlitchwave.Value;
            }
            else if (type == EffectTypes.EclipseEffect)
            {
                return DoEclipseEffect.Value;
            }
            else if (type == EffectTypes.StartMoveDustEffect) //movement effects
            {
                return DoStartMoveDustEffect.Value && DoMovementEffects.Value;
            }
            else if (type == EffectTypes.SlowDustEffect)
            {
                return DoSlowDustEffect.Value && DoMovementEffects.Value;
            }
            else if (type == EffectTypes.WalljumpDustEffect)
            {
                return DoWalljumpDustEffect.Value && DoMovementEffects.Value;
            }
            else if (type == EffectTypes.LandDustEffect)
            {
                return DoLandDustEffect.Value && DoMovementEffects.Value;
            }
            else if (type == EffectTypes.DashEffect)
            {
                return DoDashEffect.Value && DoMovementEffects.Value;
            }
            else if (type == EffectTypes.JumpDustEffect)
            {
                return DoJumpDustEffect.Value && DoMovementEffects.Value;
            }
            else if (type == EffectTypes.DoubleJumpDustEffect)
            {
                return DoDoubleJumpDustEffect.Value && DoMovementEffects.Value;
            }
            else if (type == EffectTypes.HitBallColorEffect) // ball effects
            {
                return DoHitBallColorEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.SmashEffect)
            {
                return DoSmashEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.SmallSmashEffect)
            {
                return DoSmallSmashEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.ThrowEffect)
            {
                return DoThrowEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.CatchEffect)
            {
                return DoCatchEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.HitBallEffect)
            {
                return DoHitBallEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.HitBallBehindEffect)
            {
                return DoHitBallBehindEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.WallHitEffect)
            {
                return DoWallHitEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.DeadBallEffect)
            {
                return DoDeadBallEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.HighSpeedBallReleaseEffect)
            {
                return DoHighSpeedBallReleaseEffect.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.DeflectHit) // bunt aura
            {
                return DoDeflectHit.Value && DoBallEffects.Value;
            }
            else if (type == EffectTypes.GetHitEffect)
            {
                return DoGetHitEffect.Value && DoDamageEffects.Value;
            }
            else if (type == EffectTypes.KillPlayerEffect)
            {
                return DoKillPlayerEffect.Value && DoDamageEffects.Value;
            }
            else if (type == EffectTypes.GrabFailEffect)
            {
                return DoGrabFailEffect.Value && DoDamageEffects.Value;
            }
            else if (type == EffectTypes.LightningStrikeEffect)
            {
                return DoLightningStrikeEffect.Value && DoDamageEffects.Value;
            }
            else if (type == EffectTypes.BombExplodeEffect)
            {
                return DoBombExplodeEffect.Value && DoDamageEffects.Value;
            }
            else if (type == EffectTypes.SmokeEffect)
            {
                return DoSmokeEffect.Value && DoDamageEffects.Value;
            }
            else if (type == EffectTypes.ImpactEffect)
            {
                return DoImpactEffect.Value && DoDamageEffects.Value;
            }
            else if (type == EffectTypes.ClashEffect)
            {
                return DoClashEffect.Value && DoDamageEffects.Value;
            }
            return true;
        }

        public void InitConfig(ConfigFile Config)
        {
            if (hasInit) return;

            Config.Bind(
                "General Effects",
                "mm_header_qol",
                "General Effects",
                new ConfigDescription("", null, "modmenu_header"));

            DoGlitchwave = Config.Bind<bool>(
                "Toggles",
                "Glitchwave", 
                true, 
                "Camera glitch effect when the ball is hit.");
            DoEclipseEffect = Config.Bind<bool>(
                "Toggles",
                "Eclipse",
                true,
                "Stage effects that happen at 400 speed.");

            Config.Bind("gap",
                "mm_header_gap",
                50,
                new ConfigDescription("", null, "modmenu_gap"));

            Config.Bind(
                "Movement Effects",
                "mm_header_movement",
                "Movement Effects",
                new ConfigDescription("", null, "modmenu_header"));

            DoMovementEffects = Config.Bind<bool>(
                "Toggles",
                "All Movement Effects",
                true,
                "Enable all movement effects");

            Config.Bind(
                "gap",
                "mm_header_gap2",
                25,
                new ConfigDescription("", null, "modmenu_gap")
            ); // individual movement effects

            DoStartMoveDustEffect = Config.Bind<bool>(
                "Toggles",
                "StartMoveDustEffect",
                true,
                "");

            DoSlowDustEffect = Config.Bind<bool>(
                "Toggles",
                "SlowDustEffect",
                true,
                "");

            DoWalljumpDustEffect = Config.Bind<bool>(
                    "Toggles",
                    "WalljumpDustEffect",
                    true,
                    "");

            DoLandDustEffect = Config.Bind<bool>(
                    "Toggles",
                    "LandDustEffect",
                    true,
                    "");

            DoDashEffect = Config.Bind<bool>(
                    "Toggles",
                    "DashEffect",
                    true,
                    "");

            DoJumpDustEffect = Config.Bind<bool>(
                    "Toggles",
                    "JumpDustEffect",
                    true,
                    "");

            DoDoubleJumpDustEffect = Config.Bind<bool>(
                    "Toggles",
                    "DoubleJumpDustEffect",
                    true,
                    "");


            // ball effects
            Config.Bind("gap",
                "mm_header_gap3",
                50,
                new ConfigDescription("", null, "modmenu_gap"));

            Config.Bind(
                "Ball Effects",
                "mm_header_movement",
                "Ball Effects",
                new ConfigDescription("", null, "modmenu_header"));

            DoBallEffects = Config.Bind<bool>(
                "Toggles",
                "All Ball Effects",
                true,
                "Enable all ball effects");

            Config.Bind(
                "gap",
                "mm_header_gap4",
                25,
                new ConfigDescription("", null, "modmenu_gap")
            ); 

            DoHitBallColorEffect = Config.Bind<bool>(
                    "Toggles",
                    "HitBallColorEffect",
                    true,
                    "");

            DoSmashEffect = Config.Bind<bool>(
                    "Toggles",
                    "SmashEffect",
                    true,
                    "");

            DoSmallSmashEffect = Config.Bind<bool>(
                    "Toggles",
                    "SmallSmashEffect",
                    true,
                    "");

            DoThrowEffect = Config.Bind<bool>(
                    "Toggles",
                    "ThrowEffect",
                    true,
                    "");

            DoCatchEffect = Config.Bind<bool>(
                    "Toggles",
                    "CatchEffect",
                    true,
                    "");

            DoHitBallEffect = Config.Bind<bool>(
                    "Toggles",
                    "HitBallEffect",
                    true,
                    "");

            DoHitBallBehindEffect = Config.Bind<bool>(
                    "Toggles",
                    "HitBallBehindEffect",
                    true,
                    "");

            DoWallHitEffect = Config.Bind<bool>(
                    "Toggles",
                    "WallHitEffect",
                    true,
                    "");

            DoDeadBallEffect = Config.Bind<bool>(
                    "Toggles",
                    "DeadBallEffect",
                    true,
                    "");

            DoHighSpeedBallReleaseEffect = Config.Bind<bool>(
                    "Toggles",
                    "HighSpeedBallReleaseEffect",
                    true,
                    "");


            Config.Bind("gap",
                "mm_header_gap3",
                50,
                new ConfigDescription("", null, "modmenu_gap"));

            DoDeflectHit = Config.Bind<bool>(
                    "Toggles",
                    "BuntEffect",
                    true,
                    "Bunted ball");

            // damage effects
            Config.Bind("gap",
                "mm_header_gap5",
                50,
                new ConfigDescription("", null, "modmenu_gap"));

            Config.Bind(
                "Damage Effects",
                "mm_header_movement",
                "Damage Effects",
                new ConfigDescription("", null, "modmenu_header"));

            DoDamageEffects = Config.Bind<bool>(
                "Toggles",
                "All Damage Effects",
                true,
                "Enable all damage effects");

            Config.Bind(
                "gap",
                "mm_header_gap6",
                25,
                new ConfigDescription("", null, "modmenu_gap")
            ); 

            DoGetHitEffect = Config.Bind<bool>(
                    "Toggles",
                    "GetHitEffect",
                    true,
                    "");

            DoKillPlayerEffect = Config.Bind<bool>(
                    "Toggles",
                    "KillPlayerEffect",
                    true,
                    "");

            DoGrabFailEffect = Config.Bind<bool>(
                    "Toggles",
                    "GrabFailEffect",
                    true,
                    "");

            DoLightningStrikeEffect = Config.Bind<bool>(
                    "Toggles",
                    "LightningStrikeEffect",
                    true,
                    "");

            DoBombExplodeEffect = Config.Bind<bool>(
                    "Toggles",
                    "BombExplodeEffect",
                    true,
                    "");

            DoSmokeEffect = Config.Bind<bool>(
                    "Toggles",
                    "SmokeEffect",
                    true,
                    "");

            DoImpactEffect = Config.Bind<bool>(
                    "Toggles",
                    "ImpactEffect",
                    true,
                    "");

            DoClashEffect = Config.Bind<bool>(
                    "Toggles",
                    "ClashEffect",
                    true,
                    "");


            hasInit = true;
        }
    }
}
