﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;
using BepInEx;
using BepInEx.Logging;
using EffectsSettings;
using HarmonyLib;
using LLBML.Utils;
using LLGUI;
using LLHandlers;
using Debug = UnityEngine.Debug;
using Logger = UnityEngine.Logger;

namespace EffectsSettings
{
    [BepInPlugin(PluginInfo.PLUGIN_ID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class EffectsSettingsMod : BaseUnityPlugin
    {
        internal static ManualLogSource Log { get; private set; } = null;
        public static EffectsSettings instance;

        void Awake()
        {
            Log = Logger;
            Logger.LogInfo("Patching effects settings...");

            instance = new EffectsSettings();
            instance.InitConfig(Config);

            var harmony = new Harmony(PluginInfo.PLUGIN_ID);
            harmony.PatchAll();
        }

        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info);
        }
    }
    #region Pre mod-menu code
    /*
    [HarmonyPatch(typeof(HGFCCNMEEEF), nameof(HGFCCNMEEEF.HKEABELDFML))]
    class SettingsPatch
    {
        static void Postfix(HGFCCNMEEEF __instance)
        {
            EffectsSettingsMod.Log.LogInfo("Modified Game Settings menu");
            /*
            // remove reset defaults
            __instance.CNINOFJOLNP.optionBars[__instance.CNINOFJOLNP.optionBars.Count-1].
            __instance.CNINOFJOLNP.optionBars.RemoveAt(__instance.CNINOFJOLNP.optionBars.Count - 1);
            __instance.CNINOFJOLNP.optionBars.RemoveAt(__instance.CNINOFJOLNP.optionBars.Count - 1);
            
            */
    /*
    int buttonIndex = __instance.CNINOFJOLNP.optionBars.Count;

    __instance.CNINOFJOLNP.posBarNext += __instance.CNINOFJOLNP.offsetBars; // add vertical space
    __instance.CNINOFJOLNP.AddBar(OptionsBarType.TOGGLE, "Movement Effects",EffectsPatches.moveEffect);
    __instance.CNINOFJOLNP.SetBarText(buttonIndex++, "Movement Effects");

    __instance.CNINOFJOLNP.AddBar(OptionsBarType.TOGGLE, "Ball Effects", EffectsPatches.ballEffect);
    __instance.CNINOFJOLNP.SetBarText(buttonIndex++, "Ball Effects");

    __instance.CNINOFJOLNP.AddBar(OptionsBarType.TOGGLE, "Bunt Effects", EffectsPatches.buntEffect);
    __instance.CNINOFJOLNP.SetBarText(buttonIndex++, "Bunt Effects");

    __instance.CNINOFJOLNP.AddBar(OptionsBarType.TOGGLE, "Damage Effects", EffectsPatches.damageEffect);
    __instance.CNINOFJOLNP.SetBarText(buttonIndex++, "Damage Effects");

    __instance.CNINOFJOLNP.AddBar(OptionsBarType.TOGGLE, "Eclipse Mode", EffectsPatches.eclipseMode);
    __instance.CNINOFJOLNP.SetBarText(buttonIndex++, "Eclipse Mode");

    __instance.CNINOFJOLNP.AddBar(OptionsBarType.TOGGLE, "Glitch Wave", EffectsPatches.glitchWave);
    __instance.CNINOFJOLNP.SetBarText(buttonIndex++, "Glitch Wave");

    /*
    //add reset button
    __instance.CNINOFJOLNP.AddBar(OptionsBarType.BUTTON, "...",
        Msg.SEL_RESET);
    __instance.CNINOFJOLNP.SetBarText(buttonIndex, TextHandler.Get("BT_USE_DEFAULTS"));
    */
    /*
}
}

[HarmonyPatch(typeof(BDCINPKBMBL), nameof(BDCINPKBMBL.GJGPFIOMNJN))]
class SaveDataPatch
{
static void Postfix()
{
    BDCINPKBMBL.JKEKIPIDLKN(EffectsPatches.moveEffect, 1);
    BDCINPKBMBL.JKEKIPIDLKN(EffectsPatches.ballEffect, 1);
    BDCINPKBMBL.JKEKIPIDLKN(EffectsPatches.buntEffect, 1);
    BDCINPKBMBL.JKEKIPIDLKN(EffectsPatches.damageEffect, 1);
    BDCINPKBMBL.JKEKIPIDLKN(EffectsPatches.eclipseMode, 1);
    BDCINPKBMBL.JKEKIPIDLKN(EffectsPatches.glitchWave, 1);
}
}

*/
    #endregion
}
