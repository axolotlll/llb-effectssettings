﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EffectsSettings
{
    public enum EffectTypes
    {
        GlitchwaveEffect, // QOL 1
        EclipseEffect, // QOL 2
        StartMoveDustEffect, // movement effects
        SlowDustEffect,
        WalljumpDustEffect,
        LandDustEffect,
        DashEffect,
        JumpDustEffect,
        DoubleJumpDustEffect,
        HitBallColorEffect, // ball effects
        SmashEffect,
        SmallSmashEffect,
        ThrowEffect,
        CatchEffect,
        HitBallEffect,
        HitBallBehindEffect,
        WallHitEffect,
        DeadBallEffect,
        HighSpeedBallReleaseEffect, // end ball effects
        DeflectHit, // (bunt effect)
        GetHitEffect, // start damage effects
        KillPlayerEffect, 
        GrabFailEffect,
        LightningStrikeEffect,
        BombExplodeEffect,
        SmokeEffect,
        ImpactEffect,
        ClashEffect, // end damage effects
    }
}
