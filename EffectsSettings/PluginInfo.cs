﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EffectsSettings;

#region Assembly attributes
[assembly: AssemblyVersion(PluginInfo.PLUGIN_VERSION)]
[assembly: AssemblyTitle(PluginInfo.PLUGIN_NAME + " (" + PluginInfo.PLUGIN_ID + ")")]
[assembly: AssemblyProduct(PluginInfo.PLUGIN_NAME)]

#endregion
namespace EffectsSettings
{
    public static class PluginInfo
    {
        public const string PLUGIN_NAME = "EffectsSettings";
        public const string PLUGIN_ID = "com.gitlab.axolotlll.effectssettings";
        public const string PLUGIN_VERSION = "1.0.2";
    }
}
