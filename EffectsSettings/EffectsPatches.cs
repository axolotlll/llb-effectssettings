﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using LLHandlers;
using StageBackground;

namespace EffectsSettings
{
    #region old code
    /*
    class EffectsPatches
    {
        public const HNEDEAGADKO moveEffect = (HNEDEAGADKO) 500;
        public const HNEDEAGADKO ballEffect = (HNEDEAGADKO) 501;
        public const HNEDEAGADKO buntEffect = (HNEDEAGADKO) 502;
        public const HNEDEAGADKO damageEffect = (HNEDEAGADKO) 503;
        public const HNEDEAGADKO eclipseMode = (HNEDEAGADKO) 504;
        public const HNEDEAGADKO glitchWave = (HNEDEAGADKO) 505;
    }*/
    #endregion

    /*begin movement effect*/
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateStartMoveDustEffect))]
    class StartMoveDustEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.StartMoveDustEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateSlowDustEffect))]
    class SlowDustEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.SlowDustEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateWalljumpDustEffect))]
    class WalljumpDustEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.WalljumpDustEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateLandDustEffect))]
    class LandDustEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.LandDustEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateDashEffect))]
    class DashEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.DashEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateJumpDustEffect))]
    class JumpDustEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.JumpDustEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateDoubleJumpDustEffect))]
    class DoubleJumpDustEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.DoubleJumpDustEffect);
        }
    }

    /* end movement effect*/
    /* begin hit effect */
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateHitBallColorEffect))]
    class HitBallColorEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.HitBallColorEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateSmashEffect))]
    class SmashEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.SmashEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateSmallSmashEffect))]
    class SmallSmashEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.SmallSmashEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateThrowEffect))]
    class ThrowEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.ThrowEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateCatchEffect))]
    class CatchEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.CatchEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateHitBallEffect))]
    class HitBallEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.HitBallEffect) ;
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateHitBallBehindEffect))]
    class HitBallBehindEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.HitBallBehindEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateWallHitEffect))]
    class WallHitEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.WallHitEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateDeadBallEffect))]
    class DeadBallEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.DeadBallEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateHighSpeedReleaseEffect))]
    class HighSpeedBallReleaseEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.HighSpeedBallReleaseEffect);
        }
    }
    /* end ball effect*/
    /* bunt effect *///
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateDeflectHitEffect))]
    class DeflectHit
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.DeflectHit);
        }
    }
    /*begin damage effect*/
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateGetHitEffect))]
    class GetHitEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.GetHitEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateKillPlayerEffect))]
    class KillPlayerEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.KillPlayerEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateGrabFailEffect))]
    class GrabFailEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.GrabFailEffect);
        }
    }

    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateLightningStrikeEffect))]
    class LightningStrikeEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.LightningStrikeEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateBombExplodeEffect))]
    class BombExplodeEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.BombExplodeEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateSmokeEffect))]
    class SmokeEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.SmokeEffect);
        }
    }
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateImpactEffect))]
    class ImpactEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.ImpactEffect);
        }
    }
    /*[HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateParrySuccessEffect))]
    class ParrySuccessEffect
    {
        static bool Prefix()
        {
            if (BDCINPKBMBL.CMBOEKPMJBG(EffectsPatches.damageEffect) == 0)
            {
                return false;
            }

            return true;
        }
    }*/
    [HarmonyPatch(typeof(EffectHandler), nameof(EffectHandler.CreateClashEffect))]
    class ClashEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.ClashEffect);
        }
    }
    
    /* end damage effects */

    /* start glitchwave effect
     */
    [HarmonyPatch(typeof(GameCamera), nameof(GameCamera.StartWave))]
    class StartWaveEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.GlitchwaveEffect);
        }
    }
    [HarmonyPatch(typeof(GameCamera), nameof(GameCamera.StartGlitchWave))]
    class GlitchWaveEffect
    {
        static bool Prefix()
        {
            return EffectsSettingsMod.instance.DoEffect(EffectTypes.GlitchwaveEffect);
        }
    }

    /* end glitchwave effect*/
    /* start eclipse mode*/
    [HarmonyPatch(typeof(BG), nameof(BG.SetState))]
    class EclipsePatch
    {
        static bool Prefix(BGState state)
        {
            if (!EffectsSettingsMod.instance.DoEffect(EffectTypes.EclipseEffect))
            {
                if (state == BGState.ECLIPSE || state == BGState.HEAVEN)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
